const path = require('path') // gestion fichiers locaux
const express = require('express') //framework mvc
const nunjucks = require('nunjucks') // templates
const session = require('express-session') // sessions
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
var csv = require('csv-express');

const config = require(path.join(__dirname, 'config.js'))

const TodoSchema = new mongoose.Schema({
    label: { type: String, required: true },
    dateBegin: { type: String },
    dateEnd: { type: String },
    priority: { type: String },
})

// to fix all deprecation warnings
mongoose.set('useFindAndModify', false);
mongoose.set('useNewUrlParser', true);
mongoose.set('useUnifiedTopology', true);
mongoose.set('useCreateIndex', true);

const Todo = mongoose.model('Todo', TodoSchema)

mongoose.connect('mongodb://' + config.mongodb.host + '/' + config.mongodb.db)
mongoose.connection.on('error', err => {
    console.error(err)
})

let app = express()

nunjucks.configure('views', {
    express: app
})

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

let sessionStore = new session.MemoryStore()

app.use(express.static(path.join(__dirname, '/views')))
app.use(session({
    cookie: { maxAge: 60000 },
    store: sessionStore,
    saveUninitialized: true,
    resave: 'true',
    secret: config.express.cookieSecret
}))

app.use((req, res, next) => {
    next()
})

let router = express.Router()

router.route('/')
    .get((req, res) => {
        Todo.find().then(todos => {
            res.render('todo.njk', { todos: todos })
        }).catch(err => {
            console.error(err)
        })
    })

//Ajout d'une tache
router.route('/add')
    .post((req, res) => {
        new Todo({
            label: req.body.inputLabel,
            dateBegin: req.body.inputDateBegin,
            dateEnd: req.body.inputDateEnd,
            priority: req.body.priority
        }).save().then(todo => {
            console.log('Votre tâche a été ajoutée');
            res.redirect('/todo')
        }).catch(err => {
            console.warn(err);
        })
    })

//Export en csv
router.route('/export')
    .get((req, res) => {
        var filename = "todoList.csv";
        Todo.find().lean().exec({}, function(err, products) {
            if (err) res.send(err);
            res.statusCode = 200;
            res.setHeader('Content-Type', 'text/csv');
            res.setHeader("Content-Disposition", 'attachment; filename=' + filename);
            res.csv(products, true);
        });

    });
module.exports = router;

//modification d'une tache
router.route('/edit/:id')
    .get((req, res) => {
        Todo.findById(req.params.id).then(todo => {
            res.render('edit.njk', { todo: todo })
        }).catch(err => {
            console.error(err)
        })
    })
    .post((req, res) => {
        Todo.findById(req.params.id).then(todo => {
            todo.label = req.body.inputLabel,
                todo.dateBegin = req.body.inputDateBegin,
                todo.dateEnd = req.body.inputDateEnd,
                todo.priority = req.body.priority
            todo.save().then(todo => {
                console.log('Votre tâche a été modifiée');
                res.redirect('/todo')
            }).catch(err => {
                console.error(err)
            })
        }).catch(err => {
            console.error(err)
        })
    })

//suppression de toutes les taches
router.route('/delete/all')
    .get((req, res) => {
        Todo.deleteMany().then(() => {
            console.log('Toutes les tâches ont été supprimés');
            res.redirect('/todo')
        }).catch(err => {
            console.error(err)
        })
    })

//Suppression d'une tache
router.route('/delete/:id')
    .get((req, res) => {
        Todo.findByIdAndRemove({ _id: req.params.id }).then(() => {
            console.log('Votre tâche est finie');
            res.redirect('/todo')
        }).catch(err => {
            console.error(err)
        })
    })

//Recherche d'une tache
router.route("/search")
    .get((req, res) => {
        regex = new RegExp(".*" + req.query.search + ".*", "i");
        Todo.find()
            .where('label', regex)
            .then(resQ => {
                res.render('todo.njk', { todos: resQ })
            })
            .catch((err) => {
                console.error(err);
            });
    });

app.use('/todo', router)
app.use('/pub', express.static('public'))
app.use((req, res) => {
    res.redirect('/todo')
})

app.listen(config.express.port, config.express.ip, () => {
    console.log('Server listening on ' + config.express.ip + ':' + config.express.port)
})